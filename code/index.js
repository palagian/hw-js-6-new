/* Разработайте функцию-конструктор, которая будет создавать объект Human (человек). Создайте массив объектов и 
реализуйте функцию, которая будет сортировать элементы массива по значению свойства Age по возрастанию или
убыванию */

function Human(firstName, lastName, age, password) {
    this.firstName = firstName;
    this.lastName = lastName;
    this.age = age;
    this.password = password;
};

const arr = [];

arr[0] = new Human('Vasya', 'Vasilyev', 18, 'pass1');
arr[1] = new Human('Vanya', 'Ivanov', 45, 'pass2');
arr[2] = new Human('Anya', 'Anina', 30, 'pass3');
arr[3] = new Human('Olya', 'Olina', 22, 'pass4');

const bubbleSort = array => {
    for (let i = 0; i < array.length; i++) {
        for (let j = 0; j < array.length - 1; j++) {

            if (array[j].age < array[j + 1].age) {
                const swap = array[j];

                array[j] = array[j + 1];
                array[j + 1] = swap;
            };
        };
    };
};

bubbleSort(arr);
document.write(`Сортировка людей по возрасту (от большего к меньшему): ${arr[0].firstName} ${arr[0].age}, ${arr[1].firstName} ${arr[1].age}, ${arr[2].firstName} ${arr[2].age}, ${arr[3].firstName} ${arr[3].age}. <hr/>`);

/* Разработайте функцию-конструктор, которая будет создавать объект Human (человек). Добавьте на свое усмотрение
свойства и методы в этот объект. Подумайте, какие методы и свойства следует сделать уровня экземпляра, а какие
уровня функции конструктора. */

class Human1 {
    constructor(firstName, lastName, age) {
        this.firstName;
        this.lastName;
        this.age;
    };

    promptData = () => {
        this.firstName = prompt("Введите имя");
        this.lastName = prompt("Введите фамилию");
        this.age = parseInt(prompt("Введите возраст"));
    };

    showData = () => {
        document.write(`Ваше имя ${this.firstName}.<br>`);
        document.write(`Ваша фамилия ${this.lastName}.<br>`);
        document.write(`Ваш возраст ${this.age} лет.<br>`);
    };
};

const newUser = new Human1();
newUser.promptData();
newUser.showData();
